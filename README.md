# Pipsta NFC Demonstrator Application for Android

## License
This software is licensed for use under the MIT license. See LICENSE.txt in the root directory of
the project.

## About
This application demonstrates basic interactions with both the [Pipsta](http://www.pipsta.co.uk/)
printer and the [Raspberry Pi](https://www.raspberrypi.org/) from an
[Android](https://www.android.com/) device over [NFC](http://nfc-forum.org/).

For further information please refer to the Pipsta documentation.

* [Introduction to NFC Printing on the Pipsta](https://bitbucket.org/ablesystems/pipsta/wiki/Tutorials#!nfc-printing)
* [Extending NFC Printing on the Pipsta](https://bitbucket.org/ablesystems/pipsta/wiki/Pipsta%20NFC%20Python%20Code%20Tutorial)
* [Adding Bespoke Methods to the Android Demonstrator](https://bitbucket.org/ablesystems/pipsta/wiki/Adding%20Bespoke%20Methods%20to%20the%20Pipsta%20NFC%20Android%20App)
* [Adding Bespoke Methods to the Pipsta NFC Daemon](https://bitbucket.org/ablesystems/pipsta/wiki/Adding%20Bespoke%20Methods%20to%20the%20Pipsta%20NFC%20Daemon)

## Build
This was built with version 2.1.1 of [Android Studio](https://developer.android.com/studio/index.html).
If you have any issues or questions please contact us at [support@pipsta.co.uk](mailto:support@pipsta.co.uk).