package com.pipsta.pipstanfcprinter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class PiActivity extends Activity {

    class PipstaMethod {

        String method, description, field_title, field_default;

        public PipstaMethod(String method, String description, String field_title, String field_default) {
            this.method = method;
            this.description = description;
            this.field_default = field_default;
            this.field_title = field_title;
        }

        @Override
        public String toString() {
            return this.method;
        }
    }

    private static final String TAG = "pipstanfc";
    private boolean mResumed = false;
    private boolean mWriteMode = false;
    NfcAdapter mNfcAdapter;

    PendingIntent mNfcPendingIntent;
    IntentFilter[] mWriteTagFilters;
    IntentFilter[] mNdefExchangeFilters;

    private ArrayList<PipstaMethod> pipstaMethods = new ArrayList<PipstaMethod>();
    private PipstaMethod selectedMethod;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        setContentView(R.layout.activity_pi);

        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                toast(getString(R.string.tap_on_device));

                disableNdefExchangeMode();
                enableTagWriteMode();
            }
        });

        // Handle all of our received NFC intents in this activity.
        mNfcPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // Intent filters for reading a note from a tag or exchanging over p2p.
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefDetected.addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
        }
        mNdefExchangeFilters = new IntentFilter[]{ndefDetected};

        // Intent filters for writing to a tag
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        mWriteTagFilters = new IntentFilter[]{tagDetected};
        String methods = "";
        try {
            methods = getStringFromFile(Environment.getExternalStorageDirectory() + getString(R.string.storage_folder) + getString(R.string.methods_filename));
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (String method : methods.split("\n")) {
            try {

                String methodS = method.split("method=")[1].split(",")[0];
                String descriptionS = method.split("description=\"")[1].split("\\\"")[0];
                String fieldS = method.split("field_title=\"")[1].split("\\\"")[0];
                String defaultS = method.split("field_default=\"")[1].split("\\\"")[0];

                pipstaMethods.add(new PipstaMethod(
                        methodS, descriptionS, fieldS, defaultS
                ));

            } catch (Exception e) {
                Log.e(TAG, "exception processing method " + method);
                e.printStackTrace();
            }

        }


        ArrayAdapter<PipstaMethod> spinnerArrayAdapter = new ArrayAdapter<PipstaMethod>(PiActivity.this, android.R.layout.simple_spinner_item, pipstaMethods);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ((Spinner) findViewById(R.id.spinner)).setAdapter(spinnerArrayAdapter);

        ((Spinner) findViewById(R.id.spinner)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedMethod = (PipstaMethod) adapterView.getAdapter().getItem(i);

                setupFields();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void setupFields() {

        if (selectedMethod == null) {
            return;
        }

        ((TextView) findViewById(R.id.textViewMethodDescription)).setText(selectedMethod.description);
        ((TextView) findViewById(R.id.textViewFieldName)).setText(selectedMethod.field_title);
        ((EditText) findViewById(R.id.editText1)).setHint(selectedMethod.field_default);


    }

    public static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile(String filePath) throws Exception {
        File fl = new File(filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }


    @Override
    protected void onResume() {
        super.onResume();
        mResumed = true;
        // Sticky notes received from Android
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            NdefMessage[] messages = getNdefMessages(getIntent());
            byte[] payload = messages[0].getRecords()[0].getPayload();
            ((EditText) findViewById(R.id.editText1)).setText(new String(payload));
            setIntent(new Intent()); // Consume this intent.
        }
        enableNdefExchangeMode();
    }

    private NdefMessage getNoteAsNdef() {

        if (selectedMethod == null) {
            toast("Please select a method before continuing");
            byte[] empty = new byte[]{};
            return new NdefMessage(new NdefRecord[]{
                    new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, empty)
            });
        }

        String message = ((EditText) findViewById(R.id.editText1)).getText().toString();
        if (message == null || message.length() < 1) {
            message = selectedMethod.field_default;
        }

        byte[] textBytes = String.format("method=%s,field=\"%s\"", selectedMethod.method, message).getBytes();
        byte[] messageBytes = new byte[textBytes.length + 2];

        messageBytes[0] = 0x01;
        messageBytes[1] = 0x02;

        for (int i = 2; i < messageBytes.length; i++) {
            messageBytes[i] = textBytes[i - 2];
        }

        NdefRecord textRecord = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, "text/plain".getBytes(),
                new byte[]{}, messageBytes);
        return new NdefMessage(new NdefRecord[]{
                textRecord
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        mResumed = false;
        mNfcAdapter.disableForegroundNdefPush(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // NDEF exchange mode
        if (!mWriteMode && NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            NdefMessage[] msgs = getNdefMessages(intent);
            promptForContent(msgs[0]);
        }

        // Tag writing mode
        if (mWriteMode && NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            Tag detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            writeTag(getNoteAsNdef(), detectedTag);
        }
    }

    private void promptForContent(final NdefMessage msg) {
        new AlertDialog.Builder(PiActivity.this)
                .setTitle("NFC Read")
                .setMessage(new String(msg.getRecords()[0].getPayload()))
                .setNegativeButton("OK", null)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                }).create().show();
    }


    NdefMessage[] getNdefMessages(Intent intent) {
        // Parse the intent
        NdefMessage[] msgs = null;
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            } else {
                // Unknown tag type
                byte[] empty = new byte[]{};
                NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, empty);
                NdefMessage msg = new NdefMessage(new NdefRecord[]{
                        record
                });
                msgs = new NdefMessage[]{
                        msg
                };
            }
        } else {
            Log.d(TAG, "Unknown intent.");
            finish();
        }
        return msgs;
    }


    boolean writeTag(NdefMessage message, Tag tag) {

        int size = message.toByteArray().length;

        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();

                if (!ndef.isWritable()) {
                    toast("Tag is read-only.");
                    return false;
                }
                if (ndef.getMaxSize() < size) {
                    toast("Tag capacity is " + ndef.getMaxSize() + " bytes, message is " + size
                            + " bytes.");
                    return false;
                }

                ndef.writeNdefMessage(message);
                toast(getString(R.string.write_ok));
                return true;
            } else {
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null) {
                    try {
                        format.connect();
                        format.format(message);
                        toast(getString(R.string.write_ok));
                        return true;
                    } catch (IOException e) {
                        toast("Failed to format tag.");
                        return false;
                    }
                } else {
                    toast("Tag doesn't support NDEF.");
                    return false;
                }
            }
        } catch (Exception e) {
            toast("Failed to write tag");
        }

        return false;
    }

    private void toast(String text) {
        Toast.makeText(this, text, Toast.LENGTH_LONG).show();
    }

    private void enableNdefExchangeMode() {
        mNfcAdapter.enableForegroundNdefPush(PiActivity.this, getNoteAsNdef());
        mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent, mNdefExchangeFilters, null);
    }

    private void disableNdefExchangeMode() {
        mNfcAdapter.disableForegroundNdefPush(this);
        mNfcAdapter.disableForegroundDispatch(this);
    }

    private void enableTagWriteMode() {
        mWriteMode = true;
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        mWriteTagFilters = new IntentFilter[]{
                tagDetected
        };
        mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent, mWriteTagFilters, null);
    }

    private void disableTagWriteMode() {
        mWriteMode = false;
        mNfcAdapter.disableForegroundDispatch(this);
    }


}
