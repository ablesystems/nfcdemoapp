package com.pipsta.pipstanfcprinter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class MainActivity extends Activity implements View.OnClickListener {

    NfcAdapter mNfcAdapter;
    PendingIntent mNfcPendingIntent;
    IntentFilter[] mNdefExchangeFilters;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        setContentView(R.layout.activity_main);

        findViewById(R.id.buttonAbout).setOnClickListener(this);
        findViewById(R.id.buttonPi).setOnClickListener(this);
        findViewById(R.id.buttonSendToPrinter).setOnClickListener(this);
        findViewById(R.id.buttonSettings).setOnClickListener(this);
        findViewById(R.id.imageButtonAbout).setOnClickListener(this);
        findViewById(R.id.imageButtonPi).setOnClickListener(this);
        findViewById(R.id.imageButtonSend).setOnClickListener(this);
        findViewById(R.id.imageButtonSettings).setOnClickListener(this);

        //copy able_methods if not exist
        File f = new File(Environment.getExternalStorageDirectory() + getString(R.string.storage_folder) + getString(R.string.methods_filename));

        if (!f.exists()) {

            AssetManager assetManager = getAssets();

            File dir = new File(Environment.getExternalStorageDirectory() + getString(R.string.storage_folder));
            dir.mkdir();

            String filename = getString(R.string.methods_filename);

            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(filename);
                out = new FileOutputStream(f);
                copyFile(in, out);
                in.close();
                in = null;
                out.flush();
                out.close();
                out = null;
            } catch (Exception e) {
                Log.e("tag", "Failed to copy asset file: " + filename, e);
            }

        }

        // Handle all of our received NFC intents in this activity.
        mNfcPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        // Intent filters for reading a note from a tag or exchanging over p2p.
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        try {
            ndefDetected.addDataType("text/plain");
        } catch (IntentFilter.MalformedMimeTypeException e) {
        }
        mNdefExchangeFilters = new IntentFilter[]{ndefDetected};


    }

    @Override
    protected void onResume() {
        super.onResume();
        // Sticky notes received from Android
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
            NdefMessage[] messages = getNdefMessages(getIntent());
            promptForContent(messages[0]);
            setIntent(new Intent()); // Consume this intent.
        }

        mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent, mNdefExchangeFilters, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mNfcAdapter.disableForegroundNdefPush(this);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        // NDEF exchange mode
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {
            NdefMessage[] msgs = getNdefMessages(intent);
            promptForContent(msgs[0]);
        }
    }

    private void promptForContent(final NdefMessage msg) {
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("NFC Read")
                .setMessage(new String(msg.getRecords()[0].getPayload()))
                .setNegativeButton("OK", null)
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {

                    }
                }).create().show();
    }


    NdefMessage[] getNdefMessages(Intent intent) {
        // Parse the intent
        NdefMessage[] msgs = null;
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            } else {
                // Unknown tag type
                byte[] empty = new byte[]{};
                NdefRecord record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, empty, empty);
                NdefMessage msg = new NdefMessage(new NdefRecord[]{
                        record
                });
                msgs = new NdefMessage[]{
                        msg
                };
            }
        } else {
            Log.d("MainActivity", "Unknown intent.");
            finish();
        }
        return msgs;
    }

    @Override
    public void onClick(View view) {

        Intent i = null;

        switch (view.getId()) {

            case R.id.buttonAbout:
            case R.id.imageButtonAbout:
                i = new Intent(MainActivity.this, AboutActivity.class);
                break;
            case R.id.buttonPi:
            case R.id.imageButtonPi:
                i = new Intent(MainActivity.this, PiActivity.class);
                break;
            case R.id.buttonSendToPrinter:
            case R.id.imageButtonSend:
                i = new Intent(MainActivity.this, TextActivity.class);
                break;
            case R.id.buttonSettings:
            case R.id.imageButtonSettings:
                i = new Intent(MainActivity.this, SettingsActivity.class);
                break;
        }

        if (i != null) {
            startActivity(i);
        }

    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

}
